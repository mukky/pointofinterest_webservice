<?php

use Dotenv\Dotenv;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use App\Controllers\UserController;
use DI\Container;

require __DIR__ . '/../vendor/autoload.php';

//
//$app = new Slim\App();



//(require __DIR__ . '/../config/bootstrap.php')->run();

$app = AppFactory::create();

$dotenv = Dotenv::createImmutable("../");
$dotenv->load();

$container = new Container();

// Set container to create App with on AppFactory
AppFactory::setContainer($container);
$app = AppFactory::create();


$app->add(new Tuupola\Middleware\JwtAuthentication([
    "path" => ["/api",],
    "ignore" => ["/api/v1/login","/api/v1/register","/api/v1/review/create","/api/v1/ping","/api/v1/poi_query","/api/v1/poi_details"],
    "secret" => $_ENV['SECRET_KEY'],
    "error" => function ($response, $arguments) {
        $data["status"] = "Unauthorized";
        $data["message"] = $arguments["message"];
        return $response
            ->withHeader("Content-Type", "application/json")
            ->getBody()->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
]));

$container->set('UserController', function (\Psr\Container\ContainerInterface $c) {
    return new UserController($c);
});
$container->set('POIController', function (\Psr\Container\ContainerInterface $c) {
    return new \App\Controllers\POIController($c);
});

$container->set('ReviewController', function (\Psr\Container\ContainerInterface $c) {
    return new \App\Controllers\ReviewController($c);
});

$container->set('LoginController', function (\Psr\Container\ContainerInterface $c) {
    return new \App\Controllers\LoginController($c);
});



$app->group("/api/v1/", function (\Slim\Routing\RouteCollectorProxy $group) {

    $group->post('login', \App\Controllers\LoginController::class.":login");
    $group->post('register', UserController::class.":register");


    $group->get('users', UserController::class.":index");
    $group->post('users/create', UserController::class.":create");

    $group->get('poi', \App\Controllers\POIController::class.":index");
    $group->get('poi_query', \App\Controllers\POIController::class.":query");
    $group->get('poi_details', \App\Controllers\POIController::class.":show");
    $group->post('poi/create', \App\Controllers\POIController::class.":create");

    $group->post('review/create', \App\Controllers\ReviewController::class.":create");

    $group->get("ping", function (Request $request, Response $response, $args) {

        $response->getBody()->write("Connection Available");
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    });
});


$app->run();
