<?php
namespace App\libs;
class DBConnection{
    public static function getConnection(){
        $host_name ="127.0.0.1";
        $db_user ="root";
        $db_pass ="root";
        $db_name ="aodumodu";

        try{
            $_con = new \PDO("mysql:host=$host_name;dbname=$db_name", $db_user, $db_pass);
            $_con->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $_con->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            return $_con;
        }catch (\Exception $ex)
        {
            die($ex);
            echo "Connection Failed ".$ex->getMessage();
        }


    }
}
