<?php
namespace App\Models;

use App\libs\DBConnection;

class POI{

    protected $_db;
    private $_fails;
    private $_errors;
    public function __construct() {
        $this->_db = DBConnection::getConnection();
    }


    public function validateUser($data)
    {
        $this->_errors= [];
        if(empty($data->name))
        {
            $this->_errors["name"] = "The name field is required";
            $this->_fails=true;
        }

        if(empty($data->type))
        {
            $this->_errors["type"] = "The type field is required";
            $this->_fails=true;
        }

        if(empty($data->country))
        {
            $this->_errors["country"] = "The country field is required";
            $this->_fails=true;
        }


        if(empty($data->region))
        {
            $this->_errors["region"] = "The region field is required";
            $this->_fails=true;
        }

        if(empty($data->lon))
        {
            $this->_errors["lon"] = "The lon field is required";
            $this->_fails=true;
        }

        if(empty($data->lat))
        {
            $this->_errors["lat"] = "The lat field is required";
            $this->_fails=true;
        }
        if(empty($data->description))
        {
            $this->_errors["description"] = "The description field is required";
            $this->_fails=true;
        }

    }

    public function fails()
    {
        return $this->_fails;
    }

    public function errors()
    {
        return $this->_errors;
    }


    private function filterQuery($my_array)
    {
        $allowed =["region","country","type","name"];
        $filtered = array_filter(
            $my_array,
            function ($key) use ($allowed) {
                return in_array($key, $allowed);
            },
            ARRAY_FILTER_USE_KEY
        );

        return $filtered;
    }
    public function createPOI($data) {

        $name = $data->name;
        $type = $data->type;
        $region = $data->region;
        $country = $data->country;
        $lat = $data->lat;
        $lon = $data->lon;
        $description = $data->description;

        // Gets the user into the database
        $sql = "INSERT INTO	pointsofinterest (name,type,country,region,lon,lat,description)
                VALUES		(:name,:type,:country,:region,:lon,:lat,:description)";

        $stmt = $this->_db->prepare($sql);

        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":type", $type);
        $stmt->bindParam(":region", $region);
        $stmt->bindParam(":country", $country);
        $stmt->bindParam(":lat", $lat);
        $stmt->bindParam(":lon", $lon);
        $stmt->bindParam(":description", $description);

        $stmt->execute();
        $id = $this->_db->lastInsertId();
        return (object)["id"=>$id,"name"=>$name,"type"=>$type,
            "region"=>$region,"country"=>$country,"lat"=>$lat,"lon"=>$lon,"description"=>$description];
    }


    public function fetchPOIs($request) {

        try{
            $sql = "SELECT p.ID as id, p.name, p.type,p.country,p.region,p.lon,p.lat,p.description
					FROM pointsofinterest as p
					ORDER BY p.ID DESC";
            $stmt = $this->_db->prepare($sql);

            $stmt->execute();
            $query = $stmt->fetchAll();
            $data = $query;
            return $data;
        } catch(PDOException $e) {
            echo "Error: ".$e->getMessage();
        }

    }



    public function getPoiDetails($poi_id)
    {

        $sql = "SELECT p.ID as id, p.name, p.type,p.country,p.region,p.lon,p.lat,p.description
					FROM pointsofinterest as p WHERE p.id=:poi_id";

        $stmt = $this->_db->prepare($sql);
        $stmt->bindParam(":poi_id", $poi_id);
        $stmt->execute();
        return $stmt->fetchObject();

    }


    public function queryPOIs($query_params)
    {
       $params = $this->filterQuery($query_params);
        $conditions=[];
        $parameters=[];
       foreach ($params as $index=>$value)
       {
           $conditions[] = "{$index} LIKE ?";
           $parameters[] = "%{$value}%";
       }
        $sql = "SELECT p.ID as id, p.name, p.type,p.country,p.region,p.lon,p.lat,p.description
					FROM pointsofinterest as p ";
        if ($conditions)
        {
            $sql .= " WHERE ".implode(" OR ", $conditions);
        }
        $sql .=" ORDER BY p.ID DESC";
        $stmt = $this->_db->prepare($sql);
        $stmt->execute($parameters);
        return $stmt->fetchAll();
    }


}
