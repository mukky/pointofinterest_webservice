<?php
namespace App\Models;

use App\libs\DBConnection;

class User{

    protected $_db;
    private $_fails;
    private $_errors;
    public function __construct() {
        $this->_db = DBConnection::getConnection();
    }


    public function validateUser($data)
    {
        $this->_errors= [];
        if(empty($data->username))
        {
            $this->_errors["username"] = "The username field is required";
            $this->_fails=true;
        }else{
            $username = $data->username;
            $sql = "SELECT	*
					FROM	poi_users
					WHERE	username = :username";
            $stmt = $this->_db->prepare($sql);
            $stmt->bindParam(":username", $username);
            $stmt->execute();
            $query = $stmt->fetchObject();
            if($query) {
                $this->_fails=true;
                $this->_errors["username"] = "Username has already been taken";
            }
        }

        if(empty($data->password))
        {
            $this->_errors["password"] = "The password field is required";
            $this->_fails=true;
        }

        if(!empty($data->isadmin))
        {
            if(!($data->isadmin>=0 && $data->isadmin<=1)){
                $this->_errors["isadmin"] = "The isadmin field is can only 0 or 1";
                $this->_fails=true;
            }
        }
    }

    public function validateLogin($data)
    {
        $this->_errors= [];
        if(empty($data->username))
        {
            $this->_errors["username"] = "The username field is required";
            $this->_fails=true;
        }

        if(empty($data->password))
        {
            $this->_errors["password"] = "The password field is required";
            $this->_fails=true;
        }

    }

    public function fails()
    {
        return $this->_fails;
    }

    public function errors()
    {
        return $this->_errors;
    }


    public function createUser($data) {

        $username = $data->username;
        $password = $data->password;
        $isadmin = $data->isadmin??0;

        // Gets the user into the database
        $sql = "INSERT INTO	poi_users (username, password, isadmin)
                VALUES		(:username, :password, :isadmin)";

        $stmt = $this->_db->prepare($sql);

        $stmt->bindParam(":username", $username);
        $stmt->bindParam(":password", $password);
        $stmt->bindParam(":isadmin", $isadmin);

        $stmt->execute();
        $id = $this->_db->lastInsertId();

        return (object)["id"=>$id,"username"=>$username,"isadmin"=>$isadmin];
    }

    public function updateUser($request) {
        //@var string $guid - Unique ID
        $guid = uniqid();
        // @var string $age - age
        $emp_id = $request->getParam("emp_id");
        // @var string $name - Name
        $name = $request->getParam("name");
        // @var string $email - Email
        $email = trim(strtolower($request->getParam("email")));
        // @var string $salary - salary
        $salary = $request->getParam("salary");
        // @var string $age - age
        $age = $request->getParam("age");

        try{
            $sql = "UPDATE	employee
					SET name = :name, 
						email = :email, 
						salary = :salary, 
						age = :age 
					WHERE	id = :emp_id";
            $stmt = $this->_db->prepare($sql);

            $stmt->bindParam(":name", $name);
            $stmt->bindParam(":email", $email);
            $stmt->bindParam(":salary", $salary);
            $stmt->bindParam(":age", $age);
            $stmt->bindParam(":emp_id", $emp_id);
            $result = $stmt->execute();
            if ($result) {
                $data["status"] = "Your account has been successfully updated.";
            } else {
                $data["status"] = "Error: Your account cannot be updated at this time. Please try again later.";
            }
            return $data;
        } catch(PDOException $e) {
            echo "Error: ".$e->getMessage();
        }

    }

    public function findLoginUser($username,$password) {

        $sql = "SELECT	*
					FROM	poi_users
					WHERE	username = :username AND password=:password";
        $stmt = $this->_db->prepare($sql);
        $stmt->bindParam(":username", $username);
        $stmt->bindParam(":password", $password);
        $stmt->execute();
        return $stmt->fetchObject();
    }


    public function fetchUsers($request) {

        try{
            $sql = "SELECT p.id, p.username, p.isadmin
					FROM poi_users as p
					ORDER BY p.id DESC";
            $stmt = $this->_db->prepare($sql);
           // $stmt->bindParam(":post_status", $publish);
            $stmt->execute();
            $query = $stmt->fetchAll();
            $data = $query;
            return $data;
        } catch(PDOException $e) {
            echo "Error: ".$e->getMessage();
        }

    }

    // delete emp method
    public function deleteUser($request) {
        // @var string $guid - Unique ID
        $guid = uniqid();
        $emp_id = $request->getAttribute("emp_id");
        try{
            // Delete the quote
            $sql = "DELETE FROM	employee
					WHERE id = :emp_id";
            $stmt = $this->_db->prepare($sql);
            $stmt->bindParam(":emp_id", $emp_id);
            $result = $stmt->execute();
            if ($result) {
                $data["status"] = "Your account has been successfully deleted.";
            } else {
                $data["status"] = "Error: Your account cannot be delete at this time. Please try again later.";
            }
            return $data;
        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

}
