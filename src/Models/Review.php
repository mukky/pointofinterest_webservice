<?php
namespace App\Models;

use App\libs\DBConnection;

class Review{

    protected $_db;
    private $_fails;
    private $_errors;
    public function __construct() {
        $this->_db = DBConnection::getConnection();
    }


    public function validateReview($data)
    {
        $this->_errors= [];
        if(empty($data->poi_id))
        {
            $this->_errors["poi_id"] = "The poi_id field is required";
            $this->_fails=true;
        }else{
            $poi_id = $data->poi_id;
            $sql = "SELECT	ID
					FROM pointsofinterest
					WHERE id=:poi_id";
            $stmt = $this->_db->prepare($sql);
            $stmt->bindParam(":poi_id", $poi_id);
            $stmt->execute();
            $query = $stmt->fetchObject();
            if(!$query) {
                $this->_fails=true;
                $this->_errors["poi_id"] = "poi_id does not exist";
            }
        }
        if(empty($data->review))
        {
            $this->_errors["review"] = "The review field is required";
            $this->_fails=true;
        }

    }

    public function fails()
    {
        return $this->_fails;
    }

    public function errors()
    {
        return $this->_errors;
    }


    public function createReview($data) {

        $poi_id = $data->poi_id;
        $review = $data->review;

        // Gets the user into the database
        $sql = "INSERT INTO	poi_reviews (poi_id,review)
                VALUES (:poi_id,:review)";

        $stmt = $this->_db->prepare($sql);

        $stmt->bindParam(":poi_id", $poi_id);
        $stmt->bindParam(":review", $review);
        $stmt->execute();
        $id = $this->_db->lastInsertId();

        return (object)["id"=>$id,"poi_id"=>$poi_id,"review"=>$review];
    }

    public function fetchReview($poi_id) {

        $sql = "SELECT poi_id, review
					FROM poi_reviews WHERE poi_id=:poi_id
					ORDER BY id DESC";
        $stmt = $this->_db->prepare($sql);
        $stmt->bindParam(":poi_id", $poi_id);
        $stmt->execute();
        $query = $stmt->fetchAll();
        $data = $query;
        return $data;
    }



    // delete emp method
    public function deleteUser($request) {
        // @var string $guid - Unique ID
        $guid = uniqid();
        $emp_id = $request->getAttribute("emp_id");
        try{
            // Delete the quote
            $sql = "DELETE FROM	employee
					WHERE id = :emp_id";
            $stmt = $this->_db->prepare($sql);
            $stmt->bindParam(":emp_id", $emp_id);
            $result = $stmt->execute();
            if ($result) {
                $data["status"] = "Your account has been successfully deleted.";
            } else {
                $data["status"] = "Error: Your account cannot be delete at this time. Please try again later.";
            }
            return $data;
        } catch(PDOException $e){
            echo "Error: ".$e->getMessage();
        }
    }

}
