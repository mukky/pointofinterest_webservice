<?php

namespace App\Controllers;

use App\Models\POI;
use App\Models\Review;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Psr\Container\ContainerInterface;

class ReviewController
{
    protected $container;

    private $reviewModel;

    // constructor receives container instance
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->reviewModel = new Review();
    }


    public function create(Request $request, Response $response, $args = [])
    {
        $data = json_decode($request->getBody());
        $this->reviewModel->validateReview($data);
        if($this->reviewModel->fails())
        {
            $errors = json_encode($this->reviewModel->errors());
            $response->getBody()->write($errors);
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(400);
        }

        $poi  = $this->reviewModel->createReview($data);
        $response->getBody()->write(json_encode($poi));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);
    }

}
