<?php

namespace App\Controllers;

use App\Models\User;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Psr\Container\ContainerInterface;
use Firebase\JWT\JWT;
use Tuupola\Base62;



class LoginController
{
    protected $container;
    /**
     * @var \PDO
     */
    private $connection;
    /**
     * @var \User
     */
    private $userModel;

    // constructor receives container instance
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
//        $this->connection = DBConnection::getConnection();
       $this->userModel = new User();
    }


    public function login(Request $request, Response $response, $args = [])
    {
       $data = json_decode($request->getBody());
       $this->userModel->validateLogin($data);
       if($this->userModel->fails())
       {
           $errors = json_encode($this->userModel->errors());
           $response->getBody()->write($errors);
           return $response
               ->withHeader('Content-Type', 'application/json')
               ->withStatus(400);
       }

       $user  = $this->userModel->findLoginUser($data->username,$data->password);
       if($user==null)
       {
           $errors = json_encode(["message"=>"Invalid username and password combination"]);
           $response->getBody()->write($errors);
           return $response
               ->withHeader('Content-Type', 'application/json')
               ->withStatus(400);
       }


        $base62 = new Base62();
        $jti = $base62->encode(random_bytes(128));

        $secret =$_ENV['SECRET_KEY'];

        $payload = [
            "jti" => $jti,
//            "iat" => time()+3600,
//            "nbf" =>time(),
            "user_id"=>$user->id,
            "scope"=>$user->isadmin?"admin":"user"
        ];

        $token = JWT::encode($payload, $secret, "HS256");
        $data =[
            "username"=>$user->username,
            "token"=>$token,
        ];
        $response->getBody()->write(json_encode($data));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}
