<?php

namespace App\Controllers;

use App\Models\POI;
use App\Models\Review;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Psr\Container\ContainerInterface;

class POIController
{
    protected $container;

    private $poiModel;
    /**
     * @var Review
     */
    private $reviewModel;

    // constructor receives container instance
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
       $this->poiModel = new POI();
       $this->reviewModel = new Review();
    }
    public function index(Request $request, Response $response, $args = [])
    {
        $users = $this->poiModel->fetchPOIs($request);
        $response->getBody()->write(json_encode($users));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    public function query(Request $request, Response $response, $args = [])
    {
        $params = $request->getQueryParams();
        $pois = $this->poiModel->queryPOIs($params);
        $response->getBody()->write(json_encode($pois));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }

    public function show(Request $request, Response $response, $args = [])
    {
        $poi_id = ($request->getQueryParams("poi_id"))['poi_id'];

        $poi = $this->poiModel->getPoiDetails($poi_id);
        $reviews = $this->reviewModel->fetchReview($poi->id);
        $poi->reviews = $reviews;
        $response->getBody()->write(json_encode($poi));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    public function create(Request $request, Response $response, $args = [])
    {
       $data = json_decode($request->getBody());
       $this->poiModel->validateUser($data);
       if($this->poiModel->fails())
       {
           $errors = json_encode($this->poiModel->errors());
           $response->getBody()->write($errors);
           return $response
               ->withHeader('Content-Type', 'application/json')
               ->withStatus(400);
       }

       $poi  = $this->poiModel->createPOI($data);
        $response->getBody()->write(json_encode($poi));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);
    }

}
