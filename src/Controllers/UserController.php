<?php

namespace App\Controllers;

use App\Models\User;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Psr\Container\ContainerInterface;

class UserController
{
    protected $container;
    /**
     * @var \PDO
     */
    private $connection;
    /**
     * @var \User
     */
    private $userModel;

    // constructor receives container instance
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
//        $this->connection = DBConnection::getConnection();
       $this->userModel = new User();
    }
    public function index(Request $request, Response $response, $args = [])
    {
        $users = $this->userModel->fetchUsers($request);

        $response->getBody()->write(json_encode($users));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    public function create(Request $request, Response $response, $args = [])
    {
       $data = json_decode($request->getBody());
       $this->userModel->validateUser($data);
       if($this->userModel->fails())
       {
           $errors = json_encode($this->userModel->errors());
           $response->getBody()->write($errors);
           return $response
               ->withHeader('Content-Type', 'application/json')
               ->withStatus(400);
       }

       $user  = $this->userModel->createUser($data);
        $response->getBody()->write(json_encode($user));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);
    }

    public function register(Request $request, Response $response, $args = [])
    {
        $data = json_decode($request->getBody());
        $this->userModel->validateUser($data);
        if($this->userModel->fails())
        {
            $errors = json_encode($this->userModel->errors());
            $response->getBody()->write($errors);
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(400);
        }

        $user  = $this->userModel->createUser($data);
        $response->getBody()->write(json_encode($user));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);
    }

    private function createUser(array $user): int
    {
        $row = [
            'username' => $user['username'],
            'password' => $user['password'],
            'is_admin' => $user['is_admin'],
        ];

        $sql = "INSERT INTO users SET 
                username=:username, 
                password=:password, 
                is_admin=:is_admin;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
}
